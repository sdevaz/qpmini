# QPMini

QPMini is a test application for the Quantum Pigeon interview process.

It is a single-page chat application. I aimed to have a UI that is similar to the native Messages app.

## Setup

Download dependencies (2) by running `pod install` from within the project root directory (qpmini/QPmini).

## Dependencies

- **Realm** for data persistence.
- **Kingfisher** for image downloading and caching.

## Assumptions

- I needed to create a user to represent the current "authenticated" user.
- Ideally, there would be a chat threads list view controller, where threads would be fetched and would be ready to pass to the ChatViewController (for a specific thread).

## Limitations

- You might experience a delay in saving the outgoing message due to the test's design: the message is only saved if the send message POST request was successful.
