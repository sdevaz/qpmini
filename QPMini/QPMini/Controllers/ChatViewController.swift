//
//  ChatViewController.swift
//  QPMini
//
//  Created by Sage DeVaz on 2021-09-06.
//

import Foundation
import UIKit

class ChatViewController: UIViewController {
    
    public var currentUser: QPUser!
    public var otherUser: QPUser!
    private let tableView = UITableView()
    private let inputBarView = UIView()
    private let inputContainerView = UIView()
    private let inputTextfield = UITextField()
    private let sendButton = UIButton()
    private let dataStore = DataStoreService.shared
    private var ogTableFrame = CGRect.zero // original table view frame
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        layoutUI()
        if !dataStore.messages.isEmpty {
            tableView.scrollToRow(at: IndexPath(row: dataStore.messages.count-1, section: 0), at: .bottom, animated: false)
        }
    }
}

// MARK: Setup
extension ChatViewController {
    
    private func setup() {
        view.backgroundColor = .systemGroupedBackground
        let profileView = UserAvatarView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        profileView.configure(user: otherUser)
        navigationItem.titleView = profileView
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(MessageTextCell.self, forCellReuseIdentifier: NSStringFromClass(MessageTextCell.self))
        tableView.register(MessageImageCell.self, forCellReuseIdentifier: NSStringFromClass(MessageImageCell.self))
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        tableView.keyboardDismissMode = .onDrag
        view.addSubview(tableView)
        
        inputBarView.backgroundColor = .systemGroupedBackground
        
        inputContainerView.backgroundColor = .clear
        
        sendButton.backgroundColor = .clear
        sendButton.setImage(UIImage(named: "icon_send"), for: .normal)
        sendButton.addTarget(self, action: #selector(sendButtonTapped), for: .touchUpInside)
        sendButton.isEnabled = false
        
        inputTextfield.backgroundColor = .white
        inputTextfield.borderStyle = .roundedRect
        inputTextfield.layer.masksToBounds = true
        inputTextfield.placeholder = "qpMessage"
        inputTextfield.delegate = self
        inputTextfield.returnKeyType = .done

        inputContainerView.addSubview(inputTextfield)
        inputContainerView.addSubview(sendButton)
        inputBarView.addSubview(inputContainerView)
        view.addSubview(inputBarView)
        
    }
    
    func layoutUI() {
        let spacer: CGFloat = 8.0
        let viewSize = view.bounds.size
        let viewMargins = view.safeAreaInsets
        let inputBarSize = CGSize(width: viewSize.width, height: 60)
        let inputContainerSize = CGSize(width: inputBarSize.width - 2*spacer, height: inputBarSize.height - 2*spacer)
        let tableviewSize = CGSize(width: viewSize.width, height: viewSize.height - viewMargins.bottom - inputBarSize.height - spacer)
        let sendButtonSize = CGSize(width: inputContainerSize.height, height: inputContainerSize.height)
        
        tableView.frame = CGRect(x: 0, y: 0, width: tableviewSize.width, height: tableviewSize.height)
        inputBarView.frame = CGRect(x: 0, y:  tableviewSize.height, width: inputBarSize.width, height: inputBarSize.height)
        inputContainerView.frame = CGRect(x: spacer, y: spacer, width: inputContainerSize.width, height: inputContainerSize.height)
        inputTextfield.frame = CGRect(x: 0, y: 0, width: inputContainerSize.width - sendButtonSize.width - spacer, height: inputContainerSize.height)
        sendButton.frame = CGRect(x: inputContainerSize.width - sendButtonSize.width, y: 0, width: sendButtonSize.width, height: sendButtonSize.height)
        
        // update og y positions... used for repositioning views when showing/hiding keyboard
        ogTableFrame = tableView.frame
    }
}

// MARK: Actions
extension ChatViewController {
    @objc private func sendButtonTapped() {
        
        guard let inputText = inputTextfield.text, !inputText.isEmpty else { return }
        let outgoingMessage = QPMessage(type: "text", body: inputText, authorId: currentUser.id, recipientId: otherUser.id)
        var messageCount = dataStore.messages.count
        WebService.sendMessage(message: outgoingMessage) { incomingMessage, error in
            if let error = error {
                let alert = UIAlertController(title: "Oops!", message: "Something went wrong. Give it another try! \(error.localizedDescription)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Roger that", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.inputTextfield.text = ""
            self.dataStore.create(message: outgoingMessage)
            self.tableView.insertRows(at: [IndexPath(row: messageCount, section: 0)], with: .automatic)
            if let incomingMessage = incomingMessage {
                messageCount += 1
                print(incomingMessage)
                self.dataStore.create(message: incomingMessage)
                self.tableView.insertRows(at: [IndexPath(row: messageCount, section: 0)], with: .automatic)
            }
            self.tableView.scrollToRow(at: IndexPath(row: messageCount, section: 0), at: .bottom, animated: true)
        }
    }
}

// MARK: Textfield Delegate
extension ChatViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if !text.isEmpty{
            sendButton.isEnabled = true
        } else {
            sendButton.isEnabled = false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: Keyboard Handling
extension ChatViewController {
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            inputBarView.frame.origin.y = ogTableFrame.origin.y + ogTableFrame.height + view.layoutMargins.bottom - keyboardSize.height
            tableView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: inputBarView.frame.origin.y)
            if !dataStore.messages.isEmpty {
                tableView.scrollToRow(at: IndexPath(row: dataStore.messages.count-1, section: 0), at: .bottom, animated: false)
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        tableView.frame = ogTableFrame
        inputBarView.frame.origin.y = ogTableFrame.height
    }
}

// MARK: Tableview
extension ChatViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataStore.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = dataStore.messages[indexPath.row]
        
        switch message.type {
        case "image":
            let cell = MessageImageCell(style: .default, reuseIdentifier: message.id)
            cell.configure(imageURL: message.body, width: CGFloat(message.width ?? 100), height: CGFloat(message.height ?? 100), isIncoming: message.authorId != dataStore.currentUser?.id)
            return cell
        default:
            let cell = MessageTextCell(style: .default, reuseIdentifier: message.id)
            cell.configure(text: message.body, isIncoming: message.authorId != dataStore.currentUser?.id)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
