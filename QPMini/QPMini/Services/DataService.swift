//
//  DataService.swift
//  QPMini
//
//  Created by Sage DeVaz on 2021-09-03.
//

import Foundation
import RealmSwift

// MARK: Persistent Data Service
final class DataStoreService: ObservableObject {
    
    static let shared = DataStoreService()
    private var userResults: Results<QPUserDB>
    private var messageResults: Results<QPMessageDB>
    private var realm: Realm
    
    init() {
        do {
            realm = try Realm()
            userResults = realm.objects(QPUserDB.self)
            messageResults = realm.objects(QPMessageDB.self)
        } catch let error {
            fatalError("Failed to open Realm. Error: \(error.localizedDescription)")
        }
    }
    
    var users: [QPUser] {
        userResults.map(QPUser.init)
    }
    
    var messages: [QPMessage] {
        messageResults.map(QPMessage.init)
    }
    
    func create(message: QPMessage) {
        objectWillChange.send()
        
        do {
            let realm = try Realm()
            
            let messageDB = QPMessageDB()
            messageDB.id = message.id
            messageDB.body = message.body
            messageDB.type = message.type
            messageDB.width = message.width ?? 0
            messageDB.height = message.height ?? 0
            messageDB.authorId = message.authorId
            messageDB.recipientId = message.recipientId
            messageDB.dateCreated = message.dateCreated
            
            try realm.write {
                realm.add(messageDB)
            }
        } catch let error {
            // Handle error
            print(error.localizedDescription)
        }
    }
    
    func create(user: QPUser) {
        objectWillChange.send()
        
        do {
            let realm = try Realm()
            
            let userDB = QPUserDB()
            userDB.id = user.id
            userDB.name = user.name
            userDB.imageURL = user.imageURL
            
            try realm.write {
                realm.add(userDB)
            }
        } catch let error {
            // Handle error
            print(error.localizedDescription)
        }
    }
}

// MARK: Test Environment Helpers

extension DataStoreService {

    var currentUser: QPUser? {
        userResults.map(QPUser.init).filter { $0.name == "Pigeon King"}.first
    }
    
    var otherUser: QPUser? {
        userResults.map(QPUser.init).filter { $0.id == WebService.testUserID}.first
    }
}
