//
//  WebService.swift
//  QPMini
//
//  Created by Sage DeVaz on 2021-09-07.
//

import Foundation

// MARK: Web Request Service
final class WebService {
    
    private static let baseURL = "https://us-central1-quantumpigeon-aries.cloudfunctions.net/candidate"
    
    static func sendMessage(message: QPMessage, completion: @escaping ((QPMessage?, Error?) -> Void)) {
        sendMessage(body: message.body, authorId: message.authorId, recipientId: message.recipientId, completion: completion)
    }
    
    static func sendMessage(body: String, authorId: String, recipientId: String, completion: @escaping ((QPMessage?, Error?) -> Void)) {
        let url = URL(string: baseURL + "/chat/\(recipientId)")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: ["body": body], options: []) else {
                return
        }
        request.httpBody = httpBody
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
            guard let data = data else { return }
            do {
                var responseMessage = try JSONDecoder().decode(QPMessage.self, from: data)
                responseMessage.authorId = recipientId
                responseMessage.recipientId = authorId
                DispatchQueue.main.async {
                    completion(responseMessage, nil)
                }
            } catch {
                DispatchQueue.main.async {
                    completion(nil, nil)
                }
            }
        }
        task.resume()
    }
    
    static func getUser(id: String, completion: @escaping ((QPUser?, Error?) -> Void)) {
        let url = URL(string: baseURL + "/profile/\(id)")!

        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
            guard let data = data else { return }
            do {
                let user = try JSONDecoder().decode(QPUser.self, from: data)
                DispatchQueue.main.async {
                    completion(user, nil)
                }
            } catch {
                DispatchQueue.main.async {
                    completion(nil, nil)
                }
            }
        }
        task.resume()
    }
}

// MARK: Test Environment Helpers

extension WebService {
    
    static let testUserID = "671aa2b8-0210-11ec-9a03-0242ac130003"
    
    static func getTestUser(completion: @escaping ((QPUser?, Error?) -> Void)) {
        getUser(id: testUserID, completion: completion)
    }
}
