//
//  QPUser.swift
//  QPMini
//
//  Created by Sage DeVaz on 2021-09-01.
//

import Foundation

struct QPUser: Codable {
    var id: String
    var name: String
    var imageURL: String
}

// MARK: Convenience init
extension QPUser {
    init(userDB: QPUserDB) {
        id = userDB.id
        name = userDB.name
        imageURL = userDB.imageURL
    }
}
