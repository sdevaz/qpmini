//
//  QPMessageDB.swift
//  QPMini
//
//  Created by Sage DeVaz on 2021-09-04.
//

import RealmSwift

class QPMessageDB: Object {
    @Persisted var id = ""
    @Persisted var type = ""
    @Persisted var body = ""
    @Persisted var width = 0
    @Persisted var height = 0
    @Persisted var authorId = ""
    @Persisted var recipientId = ""
    @Persisted var dateCreated = Date()
}
