//
//  QPUserDB.swift
//  QPMini
//
//  Created by Sage DeVaz on 2021-09-05.
//

import Foundation
import RealmSwift

class QPUserDB: Object {
    @Persisted var id = ""
    @Persisted var name = ""
    @Persisted var imageURL = ""
}
