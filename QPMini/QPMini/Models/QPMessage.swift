//
//  QPMessage.swift
//  QPMini
//
//  Created by Sage DeVaz on 2021-09-01.
//

import Foundation
import CoreGraphics

struct QPMessage: Codable {
    
    var id: String = UUID().uuidString
    var dateCreated = Date()
    var type: String
    var body: String
    var width: Int?
    var height: Int?
    var authorId: String = ""
    var recipientId: String = ""
    
    enum CodingKeys: String, CodingKey {
        case type, body, width, height
    }
}

// MARK: Convenience init
extension QPMessage {
  init(messageDB: QPMessageDB) {
    id = messageDB.id
    type = messageDB.type
    body = messageDB.body
    width = messageDB.width
    height = messageDB.height
    authorId = messageDB.authorId
    recipientId = messageDB.recipientId
    dateCreated = messageDB.dateCreated
  }
}
