//
//  MessageTextCell.swift
//  QPMini
//
//  Created by Sage DeVaz on 2021-09-06.
//

import Foundation
import UIKit

class MessageTextCell: UITableViewCell {
    private var label = UILabel()
    
    private let container = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: Public
extension MessageTextCell {
    public func configure(text: String, isIncoming: Bool=false) {
        label.text = text
        applyStyling(isIncoming: isIncoming)
        applyConstraints(isIncoming: isIncoming, containerWidth: contentView.bounds.size.width)
    }
}

// MARK: Helper
extension MessageTextCell {
    private func setup() {
        
        baseStyling()
        
        contentView.addSubview(container)
        container.addSubview(label)

        container.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false


    }
    
    private func applyConstraints(isIncoming: Bool, containerWidth: CGFloat) {
        
        let boundingWidth = containerWidth * 3/4
                
        if (isIncoming) {
            NSLayoutConstraint.activate([
                container.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
                container.widthAnchor.constraint(lessThanOrEqualToConstant: boundingWidth),
                container.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
                container.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
            ])
            
            NSLayoutConstraint.activate([
                label.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 8),
                label.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -8),
                label.topAnchor.constraint(equalTo: container.topAnchor, constant: 8),
                label.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -8)
            ])
            return
        }
        
        NSLayoutConstraint.activate([
            container.widthAnchor.constraint(lessThanOrEqualToConstant: boundingWidth),
            container.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            container.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            container.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])
        
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 8),
            label.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -8),
            label.topAnchor.constraint(equalTo: container.topAnchor, constant: 8),
            label.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -8)
        ])

    }
    
    private func baseStyling() {
        container.layer.cornerRadius = 14
        container.layer.masksToBounds = true
        label.numberOfLines = 0
    }
    
    private func applyStyling(isIncoming: Bool) {
        if (isIncoming) {
            container.backgroundColor = UIColor(red: 52/255.0, green: 152/255.0, blue: 219/255.0, alpha: 1.0)
            label.textColor = .white
            label.textAlignment = .left
            return
        }
        
        container.backgroundColor = .systemGray5
        label.textColor = .darkText
        label.textAlignment = .right
    }
}
