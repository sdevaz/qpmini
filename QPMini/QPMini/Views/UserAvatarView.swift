//
//  NavigationProfileView.swift
//  QPMini
//
//  Created by Sage DeVaz on 2021-09-06.
//

import Foundation
import UIKit

class UserAvatarView: UIView {
    private let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
    private let textLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

// MARK: Setup
extension UserAvatarView {
    private func setup() {
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleAspectFit
        imgView.layer.cornerRadius = 12
        imgView.layer.masksToBounds = true
        addSubview(imgView)
        
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.font = .systemFont(ofSize: 13)
        addSubview(textLabel)
        
        NSLayoutConstraint.activate([
            imgView.topAnchor.constraint(equalTo: topAnchor),
            imgView.heightAnchor.constraint(equalToConstant: 24),
            imgView.widthAnchor.constraint(equalToConstant: 24),
            imgView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
                
        NSLayoutConstraint.activate([
            textLabel.topAnchor.constraint(equalTo: imgView.bottomAnchor, constant: 2),
            textLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2),
            textLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
}

// MARK: Configuration
extension UserAvatarView {
    public func configure(user: QPUser) {
        imgView.kf.setImage(with: URL(string: user.imageURL))
        textLabel.text = user.name
    }
}
