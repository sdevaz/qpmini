//
//  MessageImageCell.swift
//  QPMini
//
//  Created by Sage DeVaz on 2021-09-06.
//

import Foundation
import UIKit
import Kingfisher

class MessageImageCell: UITableViewCell {
    private let imgView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: Setup
extension MessageImageCell {
    private func setup() {
        baseStyling()
        contentView.addSubview(imgView)
        imgView.translatesAutoresizingMaskIntoConstraints = false

    }
    
    private func applyConstraints(isIncoming: Bool, size: CGSize) {
        
        let maxSize = CGSize(width: contentView.bounds.size.width * 3/4, height: 200)
        let scaledSize = scaledSize(intrinsic: size, max: maxSize)
                        
        if (isIncoming) {
            NSLayoutConstraint.activate([
                imgView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
                imgView.widthAnchor.constraint(equalToConstant: scaledSize.width),
                imgView.heightAnchor.constraint(equalToConstant: scaledSize.height),
                imgView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
                imgView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
            ])
            return
        }
        
        NSLayoutConstraint.activate([
            imgView.widthAnchor.constraint(equalToConstant: scaledSize.width),
            imgView.heightAnchor.constraint(equalToConstant: scaledSize.height),
            imgView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            imgView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            imgView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])

    }
    
    private func baseStyling() {
        imgView.layer.cornerRadius = 14
        imgView.layer.masksToBounds = true
        imgView.contentMode = .scaleAspectFit
    }
}

// MARK: Configuration
extension MessageImageCell {
    public func configure(imageURL: String, width: CGFloat, height:CGFloat, isIncoming: Bool=false) {
        applyConstraints(isIncoming: isIncoming, size: CGSize(width: width, height: height))
        imgView.kf.setImage(with: URL(string: imageURL))
    }
}

// MARK: Helpers
extension MessageImageCell {
    func scaledSize(intrinsic size: CGSize, max maxSize: CGSize) -> CGSize {
        var width = size.width
        var height = size.height
        if size.width > maxSize.height && size.height > maxSize.height {
            if size.width/maxSize.width > size.height/maxSize.height {
                let scaleFactor = size.width / maxSize.width
                width = maxSize.width
                height = height/scaleFactor
            } else {
                let scaleFactor = size.height / maxSize.height
                height = maxSize.height
                width = width/scaleFactor
            }
        } else if size.width > maxSize.width {
            let scaleFactor = size.width / maxSize.width
            width = maxSize.width
            height = height/scaleFactor
        } else if size.height > maxSize.height {
            let scaleFactor = size.height / maxSize.height
            height = maxSize.height
            width = width/scaleFactor
        }
        
        return CGSize(width: width, height: height)
    }
}
